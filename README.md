# rcondo

![](https://img.shields.io/badge/written%20in-bash-blue)

`rcondo` is an agentless configuration management tool for Linux servers.

## About

Generate remote commands based on policy templates and execute them over SSH - no remote agent required!

Tags: sysadmin

## Usage

Rcondo first generates a remote script to run based on your host definition.
This is a script file that can include certain directives.
Any line that is not a directive is included verbatim in the generated output script.

You may then remotely execute the script on the target machine.
The `#ssh-options` directive sets execution parameters and the `--rcmd` option sets the remote interpreter (default `bash -s`).

## Generation Directives

`#include {shell-glob}`: Include other file(s) into the script.

- In the current version of `rcondo`, the glob is relative to the directory from which `rcondo` was executed, and does not otherwise change.

`#packfile {localpath} {remotepath}`: Copy a local file to the remote server.

`#packdir {localdir} {remotedir}`: Copy a local directory tree to the remote server.

Both the `packfile` and `packdir` directives generate and embed appropriate bash/tar/gzip/base64 commands within the generated script.

## Usage


```
rcondo.sh [options] file1 [file2 ...]
Options:
   -e                  Execute script remotely
   --rcmd={command}    Set remote interpreter (default 'bash -s')
   -v                  Verbose (also sets --rcmd='bash -x -s')
```


## Changelog

2015-02-20 : r76
- Enhancement: Prevent re-including same file multiple times
- Enhancement: Fatal error when including non-existent file
- Fix an issue with #packdir and #packfile directives
- policy/common.sh: Add more default packages
- policy/muninnode.sh: Suppress grep output
- policy/nodejs.sh: Follow HTTP redirects to installer
- policy/sshd.sh: Fix issue with no default authorized_keys file present
- policy/user.sh: Newly added
- [⬇️ rcondo_r76.tar.gz](dist-archive/rcondo_r76.tar.gz) *(4.56 KiB)*


2014-08-23 : r43
- Initial public release
- [⬇️ rcondo_r43.zip](dist-archive/rcondo_r43.zip) *(6.06 KiB)*

